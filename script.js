// Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
let getCube = (number) => {
    // Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
    console.log(`The cube of ${number} is ${number ** 3}`);
};
getCube(2);

// Create a variable address with a value of an array containing details of an address.
let address = ["258 Washington Ave NV", "Califonia 90011"];


// Destructure the array and print out a message with the full address using Template Literals.
let [city, country] = address;
console.log(`I live at ${city}, ${country}`);

// Create a variable animal with a value of an object data type with different animal details as it’s properties.
let animal = {
    name: "Lolong",
    type: "crocodile",
    weight: 1075,
    measurement: [20, 3]
};

// Destructure the object and print out a message with the details of the animal using Template Literals.
let {name, type, weight, measurement} = animal;
console.log(`${name} was a saltwater ${type}. He weighed at ${weight} with a measurment of ${measurement[0]} ft ${measurement[1]} in.`);

// Create an array of numbers.
let numArr = [1, 2, 3, 4, 5];

// Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
numArr.forEach(n => console.log(n)
);

// Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

let reduceNumber = numArr.reduce((acc, cur) =>acc + cur
);
console.log(reduceNumber);

// Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
class Dog{
    constructor(name, age, breed){
        this.name = name;
        this.age = age;
        this.breed = breed;
    };
};

// Create/instantiate a new object from the class Dog and console log the object.
let newDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(newDog);